using System;
using System.Collections.Generic;


namespace _2pattern
{
    using System;
    using System.Collections.Generic;

    namespace _2pattern
    {
        interface IMenuItem
        {
            string GetName();
            string GetDescription();
            double GetCost();
        }

        class Beverage : IMenuItem
        {
            private string name;
            private string description;
            private double cost;

            public Beverage(string name, string description, double cost)
            {
                this.name = name;
                this.description = description;
                this.cost = cost;
            }

            public string GetName()
            {
                return this.name;
            }

            public string GetDescription()
            {
                return this.description;
            }

            public double GetCost()
            {
                return this.cost;
            }
        }

        class Dish : IMenuItem
        {
            private string name;
            private string description;
            private double cost;

            public Dish(string name, string description, double cost)
            {
                this.name = name;
                this.description = description;
                this.cost = cost;
            }

            public string GetName()
            {
                return this.name;
            }

            public string GetDescription()
            {
                return this.description;
            }

            public double GetCost()
            {
                return this.cost;
            }
        }

        class Menu : IMenuItem
        {
            private static Menu instance;
            private List<IMenuItem> menuItems;

            private Menu()
            {
                this.menuItems = new List<IMenuItem>();
            }

            public static Menu GetInstance()
            {
                if (instance == null)
                {
                    instance = new Menu();
                }
                return instance;
            }

            public void AddItem(IMenuItem menuItem)
            {
                this.menuItems.Add(menuItem);
            }

            public string GetName()
            {
                return "Cafe Menu";
            }

            public string GetDescription()
            {
                string description = "";
                foreach (IMenuItem menuItem in this.menuItems)
                {
                    description += menuItem.GetName() + " - " + menuItem.GetDescription() + ", $" + menuItem.GetCost() + "\n";
                }
                return description;
            }

            public double GetCost()
            {
                double totalCost = 0;
                foreach (IMenuItem menuItem in this.menuItems)
                {
                    totalCost += menuItem.GetCost();
                }
                return totalCost;
            }
        }

        class Program
        {
            static void Main(string[] args)
            {
                Menu menu = Menu.GetInstance();
                menu.AddItem(new Beverage("Latte", "Espresso with steamed milk", 2.50));
                menu.AddItem(new Beverage("Cappuccino", "Espresso with equal parts steamed milk and milk foam", 2.75));
                menu.AddItem(new Dish("Cheeseburger", "Beef burger with cheese, lettuce and tomato", 4.50));
                menu.AddItem(new Dish("Fries", "Deep-fried potato sticks", 2.25));

                Console.WriteLine(menu.GetDescription());
                Console.WriteLine("Total cost: $" + menu.GetCost());

                //проверка
                Menu menu1 = Menu.GetInstance();
                Console.WriteLine(menu1.GetDescription());
                Console.WriteLine("Total cost: $" + menu.GetCost());
            }
        }
    }
}
