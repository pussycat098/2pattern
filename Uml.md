```plantuml

@startuml

interface IMenuItem {
+ GetName() : string
+ GetDescription() : string
+ GetCost() : double
}

class Beverage {
- name : string
- description : string
- cost : double
+ Beverage(name: string, description: string, cost: double)
+ GetName() : string
+ GetDescription() : string
+ GetCost() : double
}

class Dish {
- name : string
- description : string
- cost : double
+ Dish(name: string, description: string, cost: double)
+ GetName() : string
+ GetDescription() : string
+ GetCost() : double
}

class Menu {
- instance : Menu
- menuItems : List<IMenuItem>
+ Menu()
+ static GetInstance() : Menu
+ AddItem(menuItem: IMenuItem) : void
+ GetName() : string
+ GetDescription() : string
+ GetCost() : double
}

IMenuItem <|-- Beverage
IMenuItem <|-- Dish
Menu *--> IMenuItem

@enduml
```
